import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import utfpr.dainf.if62c.pratica.Jogador;

/**
 * IF62C Fundamentos de Programação 2
 * Exercício de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica71 {
    private static Scanner scn = new Scanner(System.in);
    private static final ArrayList<Jogador> time = new ArrayList<>();

    public static void main(String[] args) {
        int n;
        int numero;
        String nome;
        
        n = leInt("Número de jogadores: ");
        System.out.printf("Lendo %d jogadores:%n", n);
        for (int i = 0; i < n; i++) {
            numero = leInt("Número do jogador: ");
            nome = leString("Nome do jogador: ");
            time.add(new Jogador(numero, nome));
        }
        listaJogadores();
        Collections.sort(time);
        listaJogadores();
        
        System.out.println("\n\nINCLUIR OU ALTERAR JOGADORES");
        do {
            numero = leInt("Número: ");
            if (numero <= 0) break;
            nome = leString("Nome: ");
            Jogador j = new Jogador(numero, nome);
            int pos = Collections.binarySearch(time, j);
            if (pos < 0) {
                time.add((-pos-1), j);
            } else {
                time.set(pos, j);
            }
            listaJogadores();
        } while(numero > 0);
    }
    
    private static int leInt(String prompt) {
        int n = 0;
        boolean lido;
        do {
            System.out.print(prompt);
            lido = scn.hasNextInt();
            if (lido)
                n = scn.nextInt();
            else
                scn.next();
        } while (!lido);
        return n;
    }
    
    private static String leString(String prompt) {
        System.out.print(prompt);
        return scn.next();
    }
    
    private static void listaJogadores() {
        System.out.println("TIME:");
        for (Jogador j: time) {
            System.out.printf("%5d %s\n", j.getNumero(), j.getNome());
        }
    }
}
