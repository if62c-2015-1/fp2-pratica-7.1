Feature: Avalia a Prática 7.1 de IF62C-Fundamentos de Programação 2
    Como professor da disciplica de Fundamentos de Programação 2
    Quero avaliar a prática 7.1
    A fim de verificar a compreensão dos estudantes

    Background:
        Given the maximum grade is 100
        Given the main class is 'Pratica71'
        Given I set the script timeout to 3000
        Given I evaluate 'import utfpr.ct.dainf.if62c.pratica.*'
        Given I evaluate 'import utfpr.ct.dainf.grader.*'
        Given I evaluate 'import java.util.regex.*'
        # recarrega classes para limpar dados estáticos
        Given I evaluate 'reloadClasses()'
    
    Scenario: Verifica condições prévias
        Given I report 'Avaliando item 3...'
        Given class 'Pratica71' exists store class in <mainClass>
        And class <mainClass> has 'public' modifier
        Given class <mainClass> declares 'main(java.lang.String[])' method save in <mainMethod>    
        And member <mainMethod> has 'public' modifier
        And member <mainMethod> has 'static' modifier
        Then award 10 points

    Scenario: Verifica a leitura do número de jogadores
        Given I report 'Avaliando item 4...'
        Given I set output to <testOut>
        Given I set input from resource 'teste1.in'
        And I evaluate 'Pratica71.main(new String[0])'
        And I set input from resource 'default'
        And I set output to <default>
        And I report <testOut>
        Given <testOut> matches regex '(?s)^.*(?:Ronaldo|Julio).+(?:Julio|Ronaldo).*$'
        Then award 30 points

    Scenario: Verifica a leitura do número e nome dos jogadores
        Given I report 'Avaliando item 5...'
        Given I set input from resource 'teste2.in'
        Given I set output to <testOut>
        And I evaluate 'Pratica71.main(new String[0])'
        And I set output to <default>
        And I set input from resource 'default'
        Given <testOut> matches regex '(?s)^.*Julio.+Dunga.+Gaucho.+Zico.+Ronaldo.*$'
        Then award 30 points

    Scenario: Verifica a inclusão de 1 jogador na lista ordenada
        Given I report 'Avaliando item 6...'
        Given I set input from resource 'teste3.in'
        Given I set output to <testOut>
        And I evaluate 'Pratica71.main(new String[0])'
        And I set output to <default>
        And I set input from resource 'default'
        Given <testOut> matches regex '(?s)^.*Julio.+Dunga.+Zico.+Ronaldo.*$'
        Then award 15 points

    Scenario: Verifica a inclusão de 1 jogador e substituição de 1 jogador na lista ordenada
        Given I report 'Avaliando item 6...'
        Given I set input from resource 'teste4.in'
        Given I set output to <testOut>
        And I evaluate 'Pratica71.main(new String[0])'
        And I set output to <default>
        And I set input from resource 'default'
        Given <testOut> matches regex '(?s)^.*Julio.+Zangado.+Zico.+Ronaldo.*$'
        Then award 15 points

    Scenario: Report final grade.
        Given I report grade formatted as 'FINAL GRADE: %.1f'
